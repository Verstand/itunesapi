//
//  ViewConrollerTableViewCell.swift
//  ItunesApi
//
//  Created by Balganym on 21/02/2021.
//  Copyright © 2021 Nrd. All rights reserved.
//

import UIKit

class ViewConrollerTableViewCell: UITableViewCell {

    @IBOutlet weak var myImage: UIImageView!
    @IBOutlet weak var myLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

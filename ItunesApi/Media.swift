//
//  Media.swift
//  FirstProject
//
//  Created by Balganym on 20/02/2021.
//  Copyright © 2021 Nrd. All rights reserved.
//
import Foundation

struct Media: Codable{
    let id: Int
    var kind: String?
    let name: String
    let artwork: URL
    let genre: String
    let url: URL
    
    enum CodingKeys: String, CodingKey {
        case id = "trackId"
        case kind = "kind"
        case name = "trackName"
        case artwork = "artworkUrl100"
        case genre = "primaryGenreName"
        case url = "trackViewUrl"
    }
}


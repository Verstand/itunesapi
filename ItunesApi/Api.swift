//
//  api.swift
//  FirstProject
//
//  Created by Balganym on 21/02/2021.
//  Copyright © 2021 Nrd. All rights reserved.
//

import Foundation
import Alamofire

class Api {
        func getJsonFromMedias(medias: [Media]) -> Dictionary<String, Array<Media>>{
            var dict = Dictionary<String, Array<Media>>()
            for var media in medias{
                let kind = media.kind!
                if !dict.keys.contains(kind) {
                    dict[kind] = []
                }
                media.kind = nil
                dict[kind]!.append(media)
            }
            return dict
    //        do{
    //            let jsonData = try JSONEncoder().encode(dict)
    //            let jsonString = String(data: jsonData, encoding: String.Encoding.utf8)!
    //            print()
    //            return jsonString
    //        }catch{
    //            print(error.localizedDescription)
    //        }
    //        return ""
        }
        
        func fetchMedia(term: String, completion: @escaping (Dictionary<String, Array<Media>>?) -> Void){
        
            let parameters = ["term": term, "country": "US"]
            AF.request("https://itunes.apple.com/search", parameters: parameters)
                .response { (response) in
            
                    guard let data = response.data else { return }
                    do {
                        let decoder = JSONDecoder()
                        let mediaRequest = try decoder.decode(Medias.self, from: data)
                        let result = self.getJsonFromMedias(medias: mediaRequest.all)
    //                    print(result)
                        completion(result)
                    } catch let error {
//                        print(error)
                        completion(nil)
                    }
                }
        }
}


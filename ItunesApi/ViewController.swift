//
//  ViewController.swift
//  ItunesApi
//
//  Created by Balganym on 21/02/2021.
//  Copyright © 2021 Nrd. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var mySearchBar: UISearchBar!
    
    var data = Dictionary<String, Array<Media>>()
    let api = Api()

    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let v = UIView(frame: CGRect(x: 0, y:0, width: tableView.frame.width, height: 30))
        v.backgroundColor = .yellow
        let label = UILabel(frame: CGRect(x: 8.0, y: 4.0, width: v.bounds.size.width - 16.0, height: v.bounds.size.height - 8.0))
        label.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        label.text = Array(data.keys)[section]
        v.addSubview(label)
        return v
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 30
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        return Array(data.keys)[section].count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ViewConrollerTableViewCell
        let currentMedia = data[Array(data.keys)[indexPath.section]]![indexPath.row]
        let mediaURL = currentMedia.artwork
        let data = try? Data(contentsOf: mediaURL)
        if let imageData = data{
            cell.myImage.image = UIImage(data: imageData)
        }
        cell.myLabel.text = currentMedia.name +  "\n" + currentMedia.genre
                
        return (cell)
        
    }

    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        print("!")
        api.fetchMedia(term: searchBar.text!.lowercased(), completion: {media in
             guard let newMedia = media else{return}
             self.data = newMedia
             })
         tableView.reloadData()
    }

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mySearchBar.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view.
    }


}


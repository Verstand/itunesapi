//
//  Medias.swift
//  FirstProject
//
//  Created by Balganym on 20/02/2021.
//  Copyright © 2021 Nrd. All rights reserved.
//

import Foundation

struct Medias: Codable {
  let count: Int
  let all: [Media]
  
  enum CodingKeys: String, CodingKey {
    case count = "resultCount"
    case all = "results"
  }
}

